<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\http\Request;
use App\Models\Mon;

class MonController extends BaseController
{
    public function view_all()
    {
        $array_mon = Mon::get();
        return view('mon.view_all',compact('array_mon'));
    }
    public function view_insert()
    {
        return view('mon.view_insert');
    }
    public function process_insert(Request $rq)
    {
        Mon::create($rq->all());
        return redirect()->route('mon.view_all');
            
    }
    public function view_update($ma)
    {
        $mon = Mon::where('ma','=',$ma)->first();

    //  $khoa = Mon::find($ma);
    return view('mon.view_update', compact('mon'));
    }
    public function process_update($ma, Request $rq)
    {
        Mon::find($ma)->update($rq->all());
        return redirect()->route('mon.view_all');
    }
    public function delete($ma)
    {
        Mon::find($ma)->delete();
        return redirect()->route('mon.view_all');
    }
}
