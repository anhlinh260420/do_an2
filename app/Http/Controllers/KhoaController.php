<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\http\Request;
use App\Models\Khoa;

class KhoaController extends BaseController
{
    public function view_all()
    {
    	$array_khoa = Khoa::get();
    	return view('khoa.view_all',compact('array_khoa'));
    }
    public function view_insert()
    {
    	return view('khoa.view_insert');
    }
    public function process_insert(Request $rq)
    {
    	Khoa::create($rq->all());
    	return redirect()->route('khoa.view_all');
    		
    }
    public function view_update($ma)
    {
    	$khoa = Khoa::where('ma','=',$ma)->first();

    //	$khoa = Khoa::find($ma);
    return view('khoa.view_update', compact('khoa'));
    }
    public function process_update($ma, Request $rq)
    {
    	Khoa::find($ma)->update($rq->all());
    	return redirect()->route('khoa.view_all');
    }
    public function delete($ma)
    {
    	Khoa::find($ma)->delete();
    	return redirect()->route('khoa.view_all');
    }
}
