<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\http\Request;
use App\Models\Lop;
use App\Models\Khoa;

class LopController extends BaseController
{
    public function view_all()
    {
    	$array_lop = Lop::with('khoa')->get();
        
    

    	return view('lop.view_all',compact('array_lop'));
    }
    public function view_insert()
    {
        $array_khoa = Khoa::get();
    	return view('lop.view_insert',compact('array_khoa'));
    }
    public function process_insert(Request $rq)
    {
    	Lop::create($rq->all());
    	return redirect()->route('lop.view_all');
    		
    }
    public function view_update($ma)
    {
    	$lop = Lop::where('ma','=',$ma)->first();

    //	$lop = Lop::find($ma);
    return view('lop.view_update', compact('lop'));
    }
    public function process_update($ma, Request $rq)
    {
    	Lop::find($ma)->update($rq->all());
    	return redirect()->route('lop.view_all');
    }
    public function delete($ma)
    {
    	Lop::find($ma)->delete();
    	return redirect()->route('lop.view_all');
    }
}
