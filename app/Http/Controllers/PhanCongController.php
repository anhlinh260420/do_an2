<?php

namespace App\Http\Controllers;
use Session;
use App\Models\Admin;
use App\Models\Mon;
use App\Models\Lop;
use App\Models\PhanCong;
use Illuminate\Http\Request;

class PhanCongController extends Controller
{
    public function view_all()
    {
       if(Session::get('cap_do')==0){
 $array_phan_cong = PhanCong::where('ma_admin',Session::get('ma'))->with('admin')->with('mon')->with('lop')->get();
      
        return view('phan_cong.view_all',compact('array_phan_cong'));
       }else{
         $array_phan_cong = PhanCong::with('admin')->with('mon')->with('lop')->get();
      // return  $array_phan_cong;
        return view('phan_cong.view_all',compact('array_phan_cong'));
       }

    }

    public function view_phan_cong()
    {
        $array_mon       = Mon::get();
        $array_giao_vien = Admin::where('cap_do',0)->get();
        $array_lop       = Lop::get();

        return view('phan_cong.view_phan_cong',compact('array_mon','array_giao_vien','array_lop'));
    }
    public function process_phan_cong(Request $rq)
    {
        PhanCong::updateOrCreate([
            'ma_lop' => $rq->ma_lop,
            'ma_mon' => $rq->ma_mon,
        ],[
            'ma_admin' => $rq->ma_giao_vien
        ]);
         return redirect()->route('phan_cong.view_all');
    }
    public function delete($mon,$admin,$lop)
    {
      PhanCong::where('ma_mon',$mon)->where('ma_admin',$admin)->where('ma_lop',$lop)->delete();
      return redirect()->route('phan_cong.view_all');
    }

}

