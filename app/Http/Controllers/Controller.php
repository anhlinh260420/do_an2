<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\http\Request;
use App\Models\Admin;
use Session;
use Exception;
use App\Models\SinhVien;


class Controller extends BaseController
{
    public function master()
    {
    	return view('layout.master');
    }
    public function profile()
    {
        $info=Admin::where('ma',Session::get('ma'))->first();

        return view('profile',compact('info'));
    }

     public function update_profile()
    {
        $info=Admin::where('ma',Session::get('ma'))->first();

        return view('update_profile',compact('info'));

    }
public function process_update(Request $rq)
    {
        Admin::where('ma',$rq->ma)
        ->update(['ten'=>$rq->ten],['email'=>$rq->email],['mat_khau'=>$rq->mat_khau]);
        return redirect()->route('profile');
    }
    public function view_login(){
    if(Session::has('ma'))
    {
          return redirect()->route('home');
    }
    else{
       return view('view_login');
    }
    
    }
    public function process_login(Request $rq)
    {
    	try {
    		$admin = Admin::where('email',$rq->email)
    	->where('mat_khau',$rq->mat_khau)
    	->firstOrFail();
    	} catch (Exception $e) {
            
    		return redirect()->route('view_login')->with('error','đăng nhập sai');
    	}
    	
    	Session::put('ma',$admin->ma);
        Session::put('ten',$admin->ten);
        Session::put('cap_do',$admin->cap_do);
        return redirect()->route('home');
    }
     public function logout()
    {
        Session::flush();
       return redirect()->route('view_login')->with('error','đăng xuất thành công');
    }
    public function search(Request $rq)
    {
        $result = $rq->result;
        $result =str_replace('', '%', $result) ;
        
        $data['items']=SinhVien::where('ten','like','%'.$result.'%');
        return view('search',compact('data'));
    }
}

