<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\http\Request;
use App\Models\Admin;

class AdminController extends BaseController
{
    public function view_all()
    {
        $array_admin = Admin::get();
        return view('admin.view_all',compact('array_admin'));
    }
    public function view_insert()
    {
        return view('admin.view_insert');
    }
    public function process_insert(Request $rq)
    {
        Admin::create($rq->all());
        return redirect()->route('admin.view_all');
            
    }
    public function view_update($ma)
    {
        $admin = Admin::where('ma','=',$ma)->first();

    //  $khoa = GiaoVu::find($ma);
    return view('admin.view_update', compact('admin'));
    }
    public function process_update($ma, Request $rq)
    {
        Admin::find($ma)->update($rq->all());
        return redirect()->route('admin.view_all');
    }
    public function delete($ma)
    {
        Admin::find($ma)->delete();
        return redirect()->route('admin.view_all');
    }
}
