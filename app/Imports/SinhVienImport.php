<?php

namespace App\Imports;

use App\Models\SinhVien;
use App\Models\Lop;
use App\Models\Khoa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SinhVienImport implements ToModel, WithHeadingRow
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        foreach ($row as $key => $value) {
            if (empty($value)) {
               unset($row[$key]);
            }
        }
        if(empty($row)){
            return;
        }


        $ten=$row['ten'];
       

        $ngay_sinh = $this->convertToDate($row['ngay_sinh']);
         $dia_chi=$row['dia_chi'];
        $gioi_tinh = $this->getGioiTinh($row['gioi_tinh']);
        $sdt=$row['sdt'];
        $ma_khoa   = Khoa::firstOrCreate(['ten' => $row['khoa']])->ma;
        $ma_lop    = Lop::firstOrCreate(['ten' => $row['lop'],'ma_khoa'=>$ma_khoa])->ma;
        $sdt_phu_huynh=$row['sdt_phu_huynh'];
        return new SinhVien([
           
            'ten'       => $ten,
            'ngay_sinh' => $ngay_sinh,
            'dia_chi' => $dia_chi,
            'gioi_tinh' => $gioi_tinh,
            'sdt' => $sdt,
            'email'     => $row['email'] ?? '',
            'ma_lop'    => $ma_lop,
            'sdt_phu_huynh' => $sdt_phu_huynh,
        ]);
        
    }
    protected function convertToDate($ngay_sinh){
        return date_format(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($ngay_sinh),'Y-m-d');
    }
    protected function getGioiTinh($gioi_tinh){
        return ($gioi_tinh=='Nam') ? 1 : 0;
    }
}
