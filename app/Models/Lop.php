<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
   protected $table = 'lop';
   protected $fillable = [
   	    
    	'ten',
    	'ma_khoa',

    	
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
    	public function khoa()
{
	return $this->belongsTo('App\Models\Khoa','ma_khoa');
}
}

