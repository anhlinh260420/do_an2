<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
   protected $table = 'sinh_vien';
   protected $fillable = [
    	'ten',
    	'dia_chi',
        'ngay_sinh',
        'gioi_tinh',
        'email',
        'sdt',
        'sdt_phu_huynh',
        'ma_lop',

        
    	
    	
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
            public function lop()
{
    return $this->belongsTo('App\Models\Lop','ma_lop');
}
public function getTenGioiTinhAttribute()
{
    if ($this->gioi_tinh) {
       return "Nam"  ;  
   } 
   else {
       return "Nữ";
    }
    
}

}
