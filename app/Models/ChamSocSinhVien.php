<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChamSocSinhVien extends Model
{
   protected $table = 'cham_soc_sinh_vien';
   protected $fillable = [
   	    
    	'ghi_chu',
    	'ma_sinh_vien',
      'ngay',


    	
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
    public function sinh_vien()
{
	return $this->belongsTo('App\Models\SinhVien','ma_sinh_vien');
}
}
