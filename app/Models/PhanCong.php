<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PhanCong extends Model
{
	use Traits\HasCompositePrimaryKey;
	
    protected $table = 'phan_cong';
    public $timestamps = false;

    protected $fillable = ['ma_lop','ma_mon','ma_admin'];

    protected $primaryKey = ['ma_lop','ma_mon'];
    	public function admin()
{
	return $this->belongsTo('App\Models\Admin','ma_admin');
}
	public function mon()
{
	return $this->belongsTo('App\Models\Mon','ma_mon');
}
public function lop()
{
	return $this->belongsTo('App\Models\Lop','ma_lop');
}

}
