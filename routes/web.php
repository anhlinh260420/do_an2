<?php

Route::get('view_sinh_vien_import','SinhVienController@view_sinh_vien_import')->name('import');
Route::post('process_sinh_vien_import','SinhVienController@process_sinh_vien_import')->name('process_sinh_vien_import');


Route::get('view_login','Controller@view_login')->name('view_login');
Route::get('logout','Controller@logout')->name('logout');
Route::post('process_login','Controller@process_login')->name('process_login');

Route::group([ 'middleware' => [ 'CheckLogin' ] ], function () {
Route::get('/','Controller@master')->name('home');
Route::get('profile','Controller@profile')->name('profile');
Route::get('update_profile','Controller@update_profile')->name('update_profile');
Route::post('process_update','Controller@process_update')->name('process_profile');
Route::group(['prefix' =>'khoa','as' => 'khoa.'],function(){
Route::get('','KhoaController@view_all')->name('view_all');
Route::get('view_insert','KhoaController@view_insert')->name('view_insert');
Route::post('process_insert','KhoaController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','KhoaController@view_update')->name('view_update');
Route::post('process_update/{ma}','KhoaController@process_update')->name('process_update');
Route::get('delete/{ma}','KhoaController@delete')->name('delete');
});

Route::group(['middleware' => [ 'CheckGiaoVu' ],'prefix' =>'admin','as' => 'admin.'],function(){ 
Route::get('','AdminController@view_all')->name('view_all');
Route::get('view_insert','AdminController@view_insert')->name('view_insert');
Route::post('process_insert','AdminController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','AdminController@view_update')->name('view_update');
Route::post('process_update/{ma}','AdminController@process_update')->name('process_update');
Route::get('delete/{ma}','AdminController@delete')->name('delete');
});

Route::group(['prefix' =>'lop','as' => 'lop.'],function(){ 
Route::get('','LopController@view_all')->name('view_all');
Route::get('view_insert','LopController@view_insert')->name('view_insert');
Route::post('process_insert','LopController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','LopController@view_update')->name('view_update');
Route::post('process_update/{ma}','LopController@process_update')->name('process_update');
Route::get('delete/{ma}','LopController@delete')->name('delete');
});
Route::group(['prefix' =>'sinh_vien','as' => 'sinh_vien.'],function(){ 
Route::get('','SinhVienController@view_all')->name('view_all');
Route::get('view_insert','SinhVienController@view_insert')->name('view_insert');
Route::post('process_insert','SinhVienController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','SinhVienController@view_update')->name('view_update');
Route::post('process_update/{ma}','SinhVienController@process_update')->name('process_update');
Route::get('delete/{ma}','SinhVienController@delete')->name('delete');
});

Route::group(['prefix' =>'cham_soc_sinh_vien','as' => 'cham_soc_sinh_vien.'],function(){ 
Route::get('','ChamSocSinhVienController@view_all')->name('view_all');
Route::get('view_insert','ChamSocSinhVienController@view_insert')->name('view_insert');
Route::post('process_insert','ChamSocSinhVienController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','ChamSocSinhVienController@view_update')->name('view_update');
Route::post('process_update/{ma}','ChamSocSinhVienController@process_update')->name('process_update');
Route::get('delete/{ma}','ChamSocSinhVienController@delete')->name('delete');
});
Route::group(['prefix' =>'mon','as' => 'mon.'],function(){ 
Route::get('','MonController@view_all')->name('view_all');
Route::get('view_insert','MonController@view_insert')->name('view_insert');
Route::post('process_insert','MonController@process_insert')->name('process_insert');
Route::get('view_update/{ma}','MonController@view_update')->name('view_update');
Route::post('process_update/{ma}','MonController@process_update')->name('process_update');
Route::get('delete/{ma}','MonController@delete')->name('delete');
});
Route::group(['prefix' =>'phan_cong','as' => 'phan_cong.'],function(){ 
Route::get('','PhanCongController@view_all')->name('view_all');
Route::get('delete/{mon}/{admin}/{lop}','PhanCongController@delete')->name('delete');

Route::get('view_phan_cong','PhanCongController@view_phan_cong')->name('view_phan_cong');
Route::post('process_phan_cong','PhanCongController@process_phan_cong')->name('process_phan_cong');



});
Route::get('search','Controller@search')->name('search');
});
