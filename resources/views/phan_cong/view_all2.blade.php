@extends('layout.master')

@section('content')
<h1>
	Phân Công Giảng Dạy và Điểm Danh
</h1>

<table class="table">
	<tr>
		<th>Môn</th>
		<th>Giáo Viên</th>
		<th>Lớp</th>
		
	</tr>
	@foreach ($array_phan_cong as $phan_cong)
		<tr>
			<td>
				{{ $phan_cong->mon->ten }}
			</td>
			<td>
				{{ $phan_cong->admin->ten }}
			</td>
			<td>
				{{ $phan_cong->lop->ten }}
			</td>
			
			
		</tr>
	@endforeach
</table>
@endsection