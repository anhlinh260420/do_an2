@extends('layout.master')

@section('content')
<h1>
	Phân Công Giảng Dạy 
</h1>
@if(Session::get('cap_do')==1)
<a href="{{ route('phan_cong.view_phan_cong') }}" > thêm </a>
	@endif

<table class="table">
	<tr>
		<th>Môn</th>
		<th>Giáo Viên</th>
		<th>Lớp</th>
		@if(Session::get('cap_do')==1)
        <th>Xóa</th>
	@endif
		
	</tr>
	@foreach ($array_phan_cong as $phan_cong)
		<tr>
			<td>
				{{ $phan_cong->mon->ten }}
			</td>
			<td>
				{{ $phan_cong->admin->ten }}
			</td>
			<td>
				{{ $phan_cong->lop->ten }}
			</td>
			
			@if(Session::get('cap_do')==1)
<td><a href="{{ route('phan_cong.delete',['mon'=>$phan_cong->mon->ma,'admin'=>$phan_cong->admin->ma,'lop'=>$phan_cong->lop->ma,]) }}">Xóa</a></td>
	@endif
			
		</tr>
	@endforeach
</table>
@endsection