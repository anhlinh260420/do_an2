@extends('layout.master')

@section('content')
<form action="{{ route('phan_cong.process_phan_cong') }}" method="post">
	{{ csrf_field() }}
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}">
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn giáo viên
	<select name="ma_giao_vien">
		@foreach ($array_giao_vien as $giao_vien)
			<option value="{{ $giao_vien->ma }}">
				{{ $giao_vien->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Phân công</button>
</form>

@endsection