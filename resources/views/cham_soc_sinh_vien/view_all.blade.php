@extends('layout.master')

@section('content')
<h1>
	Chăm sóc và nhắc nhở sinh viên
</h1>

<table class="table">
	<tr>
		<th>mã</th>
		<th>Sinh Viên</th>
		<th>Ghi Chú</th>
		
		<th>Sửa</th>
		<th>Xóa</th>
		
	</tr>
	@foreach ($array_cham_soc_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ma }}
			</td>
			<td>
				{{ $sinh_vien->sinh_vien->ten }}
			</td>
			<td>
				{{ $sinh_vien->ghi_chu }}
			</td>
			
			<td>
				<a href="{{ route('cham_soc_sinh_vien.view_update',['ma'=> $sinh_vien->ma]) }}">sửa</a>
			</td>
			<td>
				<a href="{{ route('cham_soc_sinh_vien.delete',['ma'=> $sinh_vien->ma]) }}">xóa</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection