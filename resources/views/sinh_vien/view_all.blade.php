@extends('layout.master')

@section('content')
<h1>
	Quản Lý Sinh Viên
</h1>
<a href="{{ route('sinh_vien.view_insert') }}" > thêm </a><br>
<a href="{{ route('import') }}">thêm nhiều</a>
<table class="table">
	<tr>
		<th>mã</th>
		<th>khóa</th>
		<th>Lớp</th>
		<th>Tên</th>
		<th>ngày sinh</th>
		<th>địa chỉ</th>
		<th>giới tính</th>
		<th>email</th>
		<th>SĐT</th>
		<th>SĐT phụ huynh</th>
		<th>Sửa</th>
		<th>Xóa</th>
		<th>Chăm Sóc</th>
		
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ma }}
			</td>
			<td>
				{{ $sinh_vien->lop->khoa->ten }}
			</td>
			<td>
				{{ $sinh_vien->lop->ten }}
			</td>

			<td>


				{{ $sinh_vien->ten}}
			</td>
			<td>
				{{ $sinh_vien->ngay_sinh}}
			</td>
			<td>
				{{ $sinh_vien->dia_chi}}
			</td>
			<td>
				{{ $sinh_vien->ten_gioi_tinh}}
			</td>
			<td>
				{{ $sinh_vien->email}}
			</td>
			<td>
				{{ $sinh_vien->sdt}}
			</td>
			<td>
				{{ $sinh_vien->sdt_phu_huynh}}
			</td>
			<td>
				<a href="{{ route('sinh_vien.view_update',['ma'=> $sinh_vien->ma]) }}">Sửa</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.delete',['ma'=> $sinh_vien->ma]) }}">Xóa</a>
			</td>
			<td>
				<a href="{{ route('cham_soc_sinh_vien.view_insert',['ma_sinh_vien'=> $sinh_vien->ma]) }}" > Chăm Sóc </a>
			</td>
		</tr>
	@endforeach
</table>
@endsection