@extends('layout.master')

@section('content')
<form action="{{ route('process_sinh_vien_import') }}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	Lấy file mấu nếu chưa có
	<a href="https://drive.google.com/file/d/1oBCzrPYMuHrZ0gUuC0XvHZ8gYGkoa-3Q/view?usp=sharing" >File Mẫu</a>(Lưu ý : Điền Đầy Đủ Thông Tin Giống Như file mẫu)<br>
	Chọn file Excel
	<input type="file" name="file_sinh_vien" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
	<button>Nhập vào</button>
</form>

@endsection