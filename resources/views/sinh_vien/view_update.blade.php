@extends('layout.master')

@section('content')
<form action="{{ route('sinh_vien.process_update',['ma' => $sinh_vien->ma]) }}" method="post">
	{{ csrf_field()}}
	tên
	<input type="text" name="ten" value="{{ $sinh_vien->ten }}">
	<br>
	địa chỉ
	<input type="text" name="dia_chi" value="{{ $sinh_vien->dia_chi }}">
	<br>
	ngày sinh
	
	<input type="text" name="ngay_sinh" value="{{ $sinh_vien->ngay_sinh }}">
	<br>
	email
	<input type="text" name="email" value="{{ $sinh_vien->email }}">
	<br>
	sdt
	<input type="text" name="sdt" value="{{ $sinh_vien->sdt }}">
	<br>
	sdt phụ huynh
	<input type="text" name="sdt_phu_huynh" value="{{ $sinh_vien->sdt_phu_huynh }}">
	<br>
	
	giới tinh
	<input type="radio" name="gioi_tinh" value="1">Nam
	
	<input type="radio" name="gioi_tinh" value="0">Nữ
	
	
	<br>
	Lớp
		<select name="ma_lop" >
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma}}">
				{{ $lop->ten}}
			</option>
			
		@endforeach
	</select>
	<button>sua</button>

</form> 
@endsection