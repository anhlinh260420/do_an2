@extends('layout.master')

@section('content')

<form action="{{ route('sinh_vien.process_insert') }}" method="post">
	{{ csrf_field()}}
	<div>
	tên
	<input type="text" name="ten">
	<br>
	địa chỉ
	<input type="text" name="dia_chi">
	<br>
	ngày sinh
	<input type="date" name="ngay_sinh">
	<br>
	email
	<input type="text" name="email">
	<br>
	sdt
	<input type="number" name="sdt">
	<br>
	sdt phụ huynh
	<input type="number" name="sdt_phu_huynh">
	<br>
	
	giới tinh
	<input type="radio" name="gioi_tinh" value="1">Nam
	<input type="radio" name="gioi_tinh" value="0">Nữ
	<br>
	Lớp
		<select name="ma_lop" >
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma}}">
				{{ $lop->ten}}
			</option>
			
		@endforeach
	</select>
	<br>
	<button>thêm</button>
</div>

</form> @endsection