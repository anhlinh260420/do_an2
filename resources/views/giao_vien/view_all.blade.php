@extends('layout.master')

@section('content')
<h1>
	Đây là danh sách giáo viên
	
</h1>
<a href="{{ route('giao_vien.view_insert') }}" > thêm </a>
<table class="table">
	<tr>
		<th>mã</th>
		<th>Tên</th>
		<th>email</th>
		
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_giao_vien as $giao_vien)
		<tr>
			<td>
				{{ $giao_vien->ma }}
			</td>
			<td>
				{{ $giao_vien->ten}}
			</td>
			<td>
				{{ $giao_vien->email}}

			</td>
			
			<td>
				<a href="{{ route('giao_vien.view_update',['ma'=> $giao_vien->ma]) }}">sửa</a>
			</td>
			<td>
				<a href="{{ route('giao_vien.delete',['ma'=> $giao_vien->ma]) }}">xóa</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection