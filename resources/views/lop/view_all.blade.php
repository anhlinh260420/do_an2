@extends('layout.master')

@section('content')
<h1>
	Quản Lý Lớp
</h1>
<a href="{{ route('lop.view_insert') }}" > thêm </a>
<table class="table">
	<tr>
		<th>mã</th>
		<th>Tên</th>
		<th>Khóa</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_lop as $lop)
		<tr>
			<td>
				{{ $lop->ma }}
			</td>
			<td>
				{{ $lop->ten }}
			</td>
			<td>
				{{ $lop->khoa->ten }}
			</td>
			
			<td>
				<a href="{{ route('lop.view_update',['ma'=> $lop->ma]) }}">sửa</a>
			</td>
			<td>
				<a href="{{ route('lop.delete',['ma'=> $lop->ma]) }}">xóa</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection