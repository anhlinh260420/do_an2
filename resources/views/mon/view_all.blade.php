@extends('layout.master')

@section('content')
<h1>
	Quản Lý Môn
	
</h1>
<a href="{{ route('mon.view_insert') }}" > thêm </a>
<table class="table">
	<tr>
		<th>mã</th>
		<th>Tên</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_mon as $mon)
		<tr>
			<td>
				{{ $mon->ma }}
			</td>
			<td>
				{{ $mon->ten}}
			</td>
			<td>
				<a href="{{ route('mon.view_update',['ma'=> $mon->ma]) }}">sửa</a>
			</td>
			<td>
				<a href="{{ route('mon.delete',['ma'=> $mon->ma]) }}">xóa</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection