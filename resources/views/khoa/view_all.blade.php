@extends('layout.master')

@section('content')
<h1>
	Quản Lý Khóa
	
</h1>
<a href="{{ route('khoa.view_insert') }}" > thêm </a>
<table class="table">
	<tr>
		<th>mã</th>
		<th>Tên</th>
		<th>Niên Khóa</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_khoa as $khoa)
		<tr>
			<td>
				{{ $khoa->ma }}
			</td>
			<td>
				{{ $khoa->ten}}
			</td>
			<td>{{$khoa->nien_khoa}}
			</td>
			<td>
				<a href="{{ route('khoa.view_update',['ma'=> $khoa->ma]) }}">sửa</a>
			</td>
			<td>
				<a href="{{ route('khoa.delete',['ma'=> $khoa->ma]) }}">xóa</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection