<p class="menu-title sm">BROWSE <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span></p>
          <ul>
            <li class="start  open active "> <a href="{{ route('home') }}"><i class="material-icons">home</i> trang chủ  </a>

            </li>
            <li>
              <a href="{{ route('khoa.view_all') }}"> <i class="material-icons">panorama_horizontal</i> <span class="title">QL Khóa</span> </a>
            </li>
            <li>
              <a href="{{ route('lop.view_all') }}"> <i class="material-icons">email</i> QL Lớp
              </a>
            </li>
            <li>
              <a href="{{ route('sinh_vien.view_all') }}"> <i class="material-icons">invert_colors</i>QL Sinh Viên  </a>

            </li>
            <li>
              <a href="{{ route('cham_soc_sinh_vien.view_all') }}"> <i class="material-icons">airplay</i> Chăm sóc sinh viên </a>

            </li>
            <li>
               @if (Session::get('cap_do')==1)
              <a href="{{ route('admin.view_all') }}"> <i class="material-icons">flip</i>QL Admin</a>
             @endif


            </li>
           
            <li>
              <a href="{{ route('mon.view_all') }}"> <i class="material-icons">view_stream</i> QL Môn </a>

            </li>
            <li>
              <a href="{{ route('phan_cong.view_all') }}"> <i class="material-icons">flip</i> QL Phân công</a>

            </li>
