
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Quản Lý Sinh Viên</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN PLUGIN CSS -->
    <link href="{{ asset('css/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/icon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" />

    
    
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ asset('css/webarch.css') }}" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <body class="error-body no-top">
    <div class="container">
      <div class="row login-container column-seperation">
        @if(Session::has('error'))
        <h1>
          {{ Session::get('error')}}
        </h1>
        @endif
        <div class="col-md-5">
          <br>
          <form action="{{ route('process_login') }}" class="login-form validate"  method="post" name="login-form">
            {{ csrf_field()}}
            <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">Username</label>
                <input class="form-control"  name="email" type="email" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">Password</label> <span class="help"></span>
                <input class="form-control"  name="mat_khau" type="password" required>
              </div>
            </div>
            <div class="row">
              <div class="control-group col-md-10">
                <div class="checkbox checkbox check-success">
                  <a href="#">Trouble login in?</a>&nbsp;&nbsp;
                  <input id="checkbox1" type="checkbox" value="1">
                  <label for="checkbox1">Keep me reminded</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10">
                <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END CONTAINER -->
     <script src="{{ asset('js/pace.min.js') }}"type="text/javascript"></script>
    <!-- BEGIN JS DEPENDECENCIES-->
   
    <script src="{{ asset('js/jquery-1.11.3.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/jqueryblockui.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/jquery.unveil.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/jquery.scrollbar.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/jquery.animateNumbers.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/select2.min.js') }}"type="text/javascript"></script>
    <!-- END CORE JS DEPENDECENCIES-->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('js/webarch.js') }}"type="text/javascript"></script>
    <script src="{{ asset('js/chat.js') }}"type="text/javascript"></script>
    
    <!-- END CORE TEMPLATE JS -->
  </body>
</html>