@extends('layout.master')

@section('content')
<h1>
	Quản Lý Admin
	
</h1>
<a href="{{ route('admin.view_insert') }}" > thêm </a> <table class="table">
	<tr>
		<th>mã</th>
		<th>Tên</th>
		<th>email</th>
		<th>cấp độ</th>
		
		<th>Xóa</th>
	</tr>
	@foreach ($array_admin as $admin)
		<tr>
			<td>
				{{ $admin->ma }}
			</td>
			<td>
				{{ $admin->ten}}
			</td>
			<td>
				{{ $admin->email}}

			</td>
			<td>
				@if ( $admin->cap_do==1)
					Giáo vụ
				@else
					Giáo viên
				@endif
			</td>
			
			@if ($admin->cap_do==0)
				<td>
				<a href="{{ route('admin.delete',['ma'=> $admin->ma]) }}">xóa</a>
			</td>
			@else
			<td></td>
			@endif
		</tr>
	@endforeach
</table>
@endsection