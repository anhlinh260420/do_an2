<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('admin',function(Blueprint $table){
            $table->increments('ma');
            $table->string('ten',50);
            $table->string('email')->nullable()->unique();
            $table->string('mat_khau',50);
            $table->boolean('cap_do');
       }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
