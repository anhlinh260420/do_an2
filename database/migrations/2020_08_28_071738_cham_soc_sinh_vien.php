<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChamSocSinhVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cham_soc_sinh_vien',function(Blueprint $table){
            $table->increments('ma');
            $table->string('ghi_chu');
            $table->date('ngay');
            $table->integer('ma_sinh_vien')->unsigned();
            $table->foreign('ma_sinh_vien')
                ->references('ma')->on('sinh_vien')
                ->onDelete('cascade');
       });     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
