<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lop',function(Blueprint $table){
            $table->increments('ma');
            $table->string('ten',50);
            $table->integer('ma_khoa')->unsigned();
            $table->foreign('ma_khoa')
                ->references('ma')->on('khoa')
                ->onDelete('cascade');
       }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
