<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhanCong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phan_cong', function (Blueprint $table) {
            $table->integer('ma_lop')->unsigned();
            $table->foreign('ma_lop')
            ->references('ma')
            ->on('lop');
            $table->integer('ma_mon')->unsigned();
            $table->foreign('ma_mon')
            ->references('ma')
            ->on('mon');
            $table->integer('ma_admin')->unsigned();
            $table->foreign('ma_admin')
            ->references('ma')
            ->on('admin');
            $table->primary(['ma_lop','ma_mon']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phan_cong');
    }
}
