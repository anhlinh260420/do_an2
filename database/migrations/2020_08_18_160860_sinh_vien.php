<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SinhVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sinh_vien',function(Blueprint $table){
            $table->increments('ma');
            $table->string('ten',50);
            $table->date('ngay_sinh');
            $table->string('dia_chi',50);
            $table->boolean('gioi_tinh');
            $table->string('sdt',50);
            $table->string('email',50)->unique()->nullable();
            $table->integer('ma_lop')->unsigned();
            $table->foreign('ma_lop')->references('ma')->on('lop')->onDelete('cascade');
            $table->string('sdt_phu_huynh',50);   

       }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
